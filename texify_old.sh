#!/bin/bash

work_dir=$(pwd)
cd "$work_dir"
preamble="preamble.tex"
main="main.tex"

if [[ ! -e "$preamble" ]]; then
  touch "$preamble"
else
  echo "" > "$preamble"
fi

echo '\documentclass[a4paper,11pt,twoside]{article}' >> "$preamble"
echo '\title{Modelling minerals for nuclear waste storage: exploiting e-science}' >> "$preamble"
echo '\author{James C. Prime}' >> "$preamble"
echo '\date{June 2016}' >> "$preamble"
echo  >> "$preamble"

echo '\usepackage[super,sort&compress]{natbib} % Citation settings' >> "$preamble"
echo >> "$preamble"

echo '\usepackage[utf8]{inputenc} % Encoding settings' >> "$preamble"
echo '\usepackage [english]{babel} % Language settings' >> "$preamble"
echo '\usepackage{setspace} % Enables non-unity line-space settings...' >> "$preamble"
echo '\onehalfspacing % ... and sets 1.5 line spacing' >> "$preamble"
echo '%\pagenumbering{arabic} % This is the default page-numbering scheme, but just in case, this is how to set it' >> "$preamble"
echo '\usepackage [autostyle, english = american]{csquotes} % No idea why I needed this' >> "$preamble"
echo '\MakeOuterQuote{"}' >> "$preamble"
echo '%\usepackage{helvet} % Helvetica font add-in' >> "$preamble"
echo '%\renewcommand{\familydefault}{\sfdefault} % Sets Helvetica as the sans-serif font favourite' >> "$preamble"
echo  >> "$preamble"

echo '\usepackage[pdftex]{graphicx} % Graphics' >> "$preamble"
echo '\graphicspath{./} % Graphics path, shockingly' >> "$preamble"
echo  >> "$preamble"

echo '\usepackage[version=3]{mhchem} % For chemical formulae and equations and things' >> "$preamble"
echo '\usepackage{amsmath} % For mathematical equations' >> "$preamble"
echo  >> "$preamble"

echo '\usepackage{color} % Enables the marking of entries in the tables with colour' >> "$preamble"
echo '\usepackage{longtable,tabu,array,adjustbox}' >> "$preamble"
echo '\usepackage[pdfborder=000,pdftex=true]{hyperref} % This enables jumping from a reference and table of content in the pdf file to its target' >> "$preamble"
echo '\usepackage{booktabs} % Table design tool; has excellent documentation' >> "$preamble"
echo '%\usepackage{lscape} % Use this if you want to rotate the table together with the lines around the table' >> "$preamble"
echo '\usepackage[a4paper,margin=1in,footskip=.5in]{geometry}' >> "$preamble"
echo  >> "$preamble"

echo '% ########### End Preferences, Begin Document ##########' >> "$preamble"
echo  >> "$preamble"

echo '%\pagestyle{plain} % Sets no headers or footers on the first page' >> "$preamble"
echo '\usepackage{fancyhdr}' >> "$preamble"
echo '\setlength{\headheight}{20pt}' >> "$preamble"
echo '\pagestyle{fancy}' >> "$preamble"
echo '\lhead[Modelling minerals for nuclear waste storage]{James C. Prime}' >> "$preamble"
echo '%\chead[EVEN OUTPUT]{ODD OUTPUT}' >> "$preamble"
echo '\rhead[James C. Prime]{Modelling minerals for nuclear waste storage}' >> "$preamble"
echo '%\lfoot[EVEN OUTPUT]{ODD OUTPUT}' >> "$preamble"
echo '\cfoot[\thepage]{\thepage}' >> "$preamble"
echo '%\rfoot[EVEN OUTPUT]{ODD OUTPUT}' >> "$preamble"
echo  >> "$preamble"

# Create main.tex to call all other files
if [[ ! -e "$main" ]]; then
  touch "$main"
else
  echo "" > "$main"
fi

# This is to go in the main document rather than the preamble, actually
echo '\include{./preamble.tex}' >> "$main"
echo  >> "$main"
echo '\begin{document}' >> "$main"
echo '\bibliographystyle{unsrtnat}' >> "$main"
echo '%\renewcommand{\baselinestretch}{1.5}' >> "$main"
echo '%\setmainfont{Arial}' >> "$main"
echo  >> "$main"
echo '\include{./introduction.tex}' >> "$main"
echo '\include{./literature_review.tex}' >> "$main"
echo '\include{./methodology.tex}' >> "$main"
echo '\include{./original_contribution.tex}' >> "$main"
echo '\include{./plan_timetable.tex}' >> "$main"
echo '\bibliography{~/Desktop/PhD/PhD}' >> "$main"
echo '\end{document}' >> "$main"
